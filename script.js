var draggables = document.querySelectorAll("#container > div:not(.absolute)");
var centerCircle = document.querySelector("#center-circle");
var tinySwatches = document.querySelectorAll("#center-circle > div > div");
var latestColors = [];
draggables.forEach(function (draggable) {
    draggable.draggable = true;
    draggable.addEventListener("dragstart", function (e) {
        draggable.classList.add("dragging");
        centerCircle.classList.add("dragging");
        e.dataTransfer.setData("text/plain", draggable.id);
    });
    draggable.addEventListener("dragend", function (e) {
        draggable.classList.remove("dragging");
        centerCircle.classList.remove("dragging");
    });
});
function updateSwatches() {
    switch (latestColors.length) {
        case 0:
            tinySwatches.forEach(function (value) { return value.hidden = true; });
            break;
        case 1:
            tinySwatches.forEach(function (value) { return value.hidden = true; });
            tinySwatches[0].hidden = false;
            tinySwatches[0].style.backgroundColor = latestColors[0];
            break;
        case 2:
            latestColors.forEach(function (value, i) {
                tinySwatches[i].style.backgroundColor = value;
                tinySwatches[i].hidden = false;
            });
    }
}
function updateCircleColor() {
    if (latestColors.length === 0) {
        centerCircle.style.backgroundColor = "white";
    }
    else if (latestColors.length === 1) {
        centerCircle.style.backgroundColor = "".concat(latestColors[0]);
    }
    else {
        centerCircle.style.backgroundColor = "color-mix(in srgb, ".concat(latestColors[0], ", ").concat(latestColors[1], ")");
    }
    updateSwatches();
}
centerCircle.addEventListener("dragenter", function (e) {
    e.preventDefault();
    centerCircle.classList.add("dragover");
});
centerCircle.addEventListener("dragover", function (e) {
    e.preventDefault();
});
centerCircle.addEventListener("dragleave", function (e) {
    centerCircle.classList.remove("dragover");
});
centerCircle.addEventListener("drop", function (e) {
    centerCircle.classList.remove("dragover");
    var data = e.dataTransfer.getData("text/plain");
    if (data === "clear") {
        latestColors = [];
        updateCircleColor();
        return;
    }
    if (latestColors.length === 2)
        latestColors = [latestColors[1], data];
    else
        latestColors.push(data);
    updateCircleColor();
});
updateCircleColor();
